const express = require('express')
const exphbs = require('express-handlebars')
let mysql = require('mysql')
const app = express()

let connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'bussury',
  database: 'zancars'
})
connection.connect()

app.engine('handlebars', exphbs({
  defaultLayout: 'main'
}))
app.set('view engine', 'handlebars')

// How middleware
app.use((req, res, next) => {
  // console.log('middleware')
  next()
})
// Index routes
app.get('/', (req, res) => {
  let vm = this
  connection.query('SELECT * FROM locations', (error, rows) => {
    if (error) {
      console.log(error)
    }
    vm.data = rows
  })
  let title = 'home page'
  res.render('index', {
    title: title,
    locations: vm.data
  })
})
app.get('/about', (req, res) => {
  let title = 'About page'
  res.render('about', {
    title: title
  })
})

const port = 5000
app.listen(port, (req, res) => {
  console.log(`Server is running from port ${port}`)
})
